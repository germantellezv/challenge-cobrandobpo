# Test - Cobrando BPO

Backen development test

## DEMO
[http://cobrandobpo.herokuapp.com/](http://cobrandobpo.herokuapp.com/)

## Requirements

```
Node.js
```


## Install

Create a project folder
```
mkdir test
```
Clone repository
```
git clone git@gitlab.com:germantellezv/challenge-cobrandobpo.git test/
```
Open project folder
```
cd test
```

Install dependencies
```
npm install
```
## Run
Run the app
```
npm start
```


## Tech Stack

```
Node.js
Express.js
Passport.js
MongoDB Atlas
Bootstrap
```

## License (MIT)

This project is licensed under the MIT license [[READ LICENSE](./LICENSE)]