const mongoose = require('mongoose')
mongoose.Promise = global.Promise;

async function connect() {
  const url = process.env.DB_URI
  await mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true})
  console.log('[db] Conexión exitosa.');
}


module.exports = {
  connect,
};