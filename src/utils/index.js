module.exports = {
  isLoggedIn: function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()){
      return next()
    }
    return res.redirect('/auth/login')
  },
  getAllColumnsFromObjs: function getAllColumnsFromObjs(allRows) {
    if (allRows.length > 0) {
      let allColumns = []
      for (let i = 0; i < allRows.length; i++) {
        const obj = allRows[i];
        const columnsList = Object.keys(JSON.parse(JSON.stringify(obj)))
        columnsList.pop()
        columnsList.shift()
        
        for (let j = 0; j < columnsList.length; j++) {
          const column = columnsList[j];
          if (i == 0) {
            allColumns.push(column)
          } else {
            if (!allColumns.includes(column)) {
              allColumns.push(column)
            }
          }
        }
      }

      console.log(allColumns);
    }
  }
}