const home = require("../components/home/network");
const dashboard = require("../components/dashboard/network");

const router = function (app, passport) {
  app.get("/", (req, res) => {
    res.redirect("/auth/login");
  });
  
  app.use("/auth", home(passport));
  app.use("/dashboard", dashboard);
};

module.exports = router;
