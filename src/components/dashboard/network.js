const { json } = require("body-parser");
const express = require("express");
const { isLoggedIn } = require("../../utils/index");
const { process_with_xlsx } = require('./controller')
const store = require('./store')

const router = express.Router();

router.get("/upload", [isLoggedIn], (req, res) => {
  res.render("dashboard", {
    user: req.user,
    errorMessage: req.flash("failedUploadMessage"),
    message: req.flash("uploadMessage"),
  });
});

router.post("/upload", [isLoggedIn], async (req, res) => {
  
  const xls_mimetype = "application/vnd.ms-excel"
  const xlsx_mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  const file = req.files.excelFile;
  const filePath = "uploads" + file.name;

  if (!req.files || Object.keys(req.files).length === 0 ||
    req.files.excelFile.mimetype !== xlsx_mimetype)
    {
      req.flash("failedUploadMessage","Intentalo de nuevo con un archivo de Excel válido.");
      return res.redirect("/dashboard/upload");
    }

  if (file.mimetype === xlsx_mimetype) {
    await process_with_xlsx(file, filePath, req, res)
  }
  
  req.flash("uploadMessage", "Archivo procesado exitosamente.");
  return res.redirect("/dashboard/upload");
});

router.get('/list', [isLoggedIn], async (req, res) => {
  
  const result = await store.list()
  res.status(200).send(result)
})

router.post('/list', [isLoggedIn], async (req, res) => {
  const result = await store.deleteAll()
  req.flash('uploadMessage','[DELETE] Operación realizada exitosamente')
  res.redirect('/dashboard/upload')
})

module.exports = router;
