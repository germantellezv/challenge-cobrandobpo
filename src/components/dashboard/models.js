const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const dataSchema = new mongoose.Schema({}, {strict: false})

module.exports = mongoose.model('Data', dataSchema)