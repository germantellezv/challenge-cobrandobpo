let xlsxtojson = require("xlsx-to-json-lc");
const { getAllColumnsFromObjs } = require('../../utils/index')
const fs = require('fs-extra')
const util = require('util')
const Data = require('./models');

function process_with_xlsx(file, filePath, req, res) {

  return new Promise((resolve, reject) => {
    file.mv(filePath, function (err) {
      return new Promise((resolve, reject) => {
        if (err) {
          reject(err);
        }
        resolve();
      }).then( async () => {
        await read_xlsx_file(filePath, req, res).then(result => {
          resolve(result)
        })
      })
      .catch((err) => {
        console.error(err);
      });
    })
  })



}

function read_xlsx_file(filePath, req, res) {
  
  return new Promise((resolve, reject) => {

    xlsxtojson(
      {
        input: filePath,
        output: null,
        lowerCaseHeaders: true
      }, async function (err, result) {
        try {
          await fs.remove(filePath);
          const allRows = await Data.find({});
          const upload = await Data.insertMany(result)
          resolve(result)
        } catch (error) {
          console.log(error);
        }
      })
  })

    
  

  
}


module.exports = {
  process_with_xlsx,
}