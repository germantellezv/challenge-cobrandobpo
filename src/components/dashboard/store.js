const Data = require('./models')

const list = async () => {
  const allRows = await Data.find();
  return allRows;
}

const deleteAll = async () => {
  const result = await Data.deleteMany({});
  return result;
}

module.exports = {
  list,
  deleteAll
}