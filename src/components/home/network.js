// const passport = require('../../server');
const { isLoggedIn } = require("../../utils/index");
const express = require("express");
const router = express.Router();

let home = (passport) => {
  
  router.get("/login", (req, res) => {
    res.render("login", {
      message: req.flash("loginMessage"),
    });
  });
  
  router.post(
    "/login",
    passport.authenticate("local-login", {
      successRedirect: "/dashboard/upload",
      failureRedirect: "/auth/login",
      failureFlash: true,
    })
  );
  
  router.get("/signup", (req, res) => {
    res.render("signup", {
      message: req.flash("signupMessage"),
    });
  });
  
  router.post(
    "/signup",
    passport.authenticate("local-signup", {
      successRedirect: "/dashboard/upload",
      failureRedirect: "/auth/signup",
      failureFlash: true,
    })
  );
  
  router.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/auth/login");
  });

  return router
}

module.exports = home